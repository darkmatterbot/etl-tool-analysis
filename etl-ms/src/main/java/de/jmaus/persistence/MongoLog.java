package de.jmaus.persistence;

import java.time.Instant;

import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.quarkus.mongodb.panache.common.MongoEntity;
import lombok.Data;

@Data
@MongoEntity(collection = "logs")
public class MongoLog extends PanacheMongoEntity {

    public String  documentId;

    public Instant createdAt;

    public String  message;

    public String  value;
	
    public static MongoLog findByDocumentId(String id) {
        return find("documentId", id).firstResult();
    }

}
