package de.jmaus.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
public class IdentifierChangeJson extends BaseJson{
	
	@JsonProperty("kundenId")
	private Long kundenId;

	@JsonProperty("bestellId")
	private Long bestellId;
}
