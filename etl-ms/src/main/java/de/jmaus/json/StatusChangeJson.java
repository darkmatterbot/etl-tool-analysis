package de.jmaus.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
public class StatusChangeJson extends BaseJson {

	@JsonProperty("itemId")
	private String itemId;

	@JsonProperty("status")
	private Long status;
}
