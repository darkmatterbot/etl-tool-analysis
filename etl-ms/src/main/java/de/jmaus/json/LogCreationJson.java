package de.jmaus.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
public class LogCreationJson extends BaseJson {

	@JsonProperty("documentId")
	private String documentId;

	@JsonProperty("message")
	private String message;

	@JsonProperty("value")
	private String value;

}
