package de.jmaus;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import de.jmaus.json.IdentifierChangeJson;
import de.jmaus.json.LogCreationJson;
import de.jmaus.json.StatusChangeJson;
import de.jmaus.kafka.schema.IdentifierStateTO;
import de.jmaus.kafka.schema.LogCreationMessageTO;
import de.jmaus.kafka.schema.StatusChangeTO;
import de.jmaus.persistence.Auftrag;
import de.jmaus.persistence.MongoLog;
import de.jmaus.persistence.StatusEntity;
import io.quarkus.logging.Log;
import io.smallrye.common.annotation.Blocking;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;

@ApplicationScoped
public class ConsumerProcessor {

	static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSZ");

	@RestClient
	HttpReceiverClient httpReceiver;

	@Blocking
	@Incoming("log-creation-message")
	public void consumeLogCreation(List<LogCreationMessageTO> tos) {
		List<LogCreationJson> jsons = new ArrayList<>();

		for (LogCreationMessageTO to : tos) {
			LogCreationJson j = new LogCreationJson();
			j.setProcessStart(Instant.now().toEpochMilli());

			// start work
			j.setCreatedAt(to.getCreatedAt().toEpochMilli());
			j.setDocumentId(to.getDocumentId());
			j.setMessage(to.getMessage());
			j.setMessageCount(to.getMessageCount());
			j.setProcessor("etl-ms");

			MongoLog entity = MongoLog.findByDocumentId(to.getDocumentId());
			if (entity != null) {
				j.setValue(entity.getValue());
			}
			j.setProcessedAt(Instant.now().toEpochMilli());
			// end work
			jsons.add(j);
		}
		try {
			httpReceiver.postLogCreationMessage(jsons);
		} catch (Exception e) {
			Log.error(e);
		}
	}

	@Blocking
	@Transactional
	@Incoming("identifier-change-event")
	public void consumeIdentifierChange(List<IdentifierStateTO> tos) {
		List<IdentifierChangeJson> jsons = new ArrayList<>();
		for (IdentifierStateTO to : tos) {

			IdentifierChangeJson json = new IdentifierChangeJson();
			json.setProcessStart(Instant.now().toEpochMilli());

			// Start work
			long kundenId = to.getKundenId();
			long bestellId = to.getBestellId();
			json.setCreatedAt(to.getCreatedAt().toEpochMilli());
			json.setKundenId(kundenId);
			json.setBestellId(bestellId);
			json.setProcessor("etl-ms");
			json.setMessageCount(to.getMessageCount());

			Auftrag e = Auftrag.findByKundenId(kundenId);
			if (e != null && e.getBestellid() == null) {
				e.setBestellid(bestellId);
				e.persist();
			} else if (e != null && e.getKundenid() == null) {
				e.setKundenid(kundenId);
				e.persist();
			}
			json.setProcessedAt(Instant.now().toEpochMilli());
			// End Work
			jsons.add(json);
		}
		try {
			httpReceiver.postIdentifierChange(jsons);
		} catch (Exception e) {
			Log.error(e);
		}
	}

	@Blocking
	@Transactional
	@Incoming("status-change-event")
	public void consumeStatusChange(List<StatusChangeTO> tos) {
		List<StatusChangeJson> jsons = new ArrayList<>();
		for (StatusChangeTO to : tos) {
			StatusChangeJson json = new StatusChangeJson();
			json.setProcessStart(Instant.now().toEpochMilli());

			// Start work
			String itemId = to.getItemId();
			Long status = to.getStatus();
			json.setCreatedAt(to.getCreatedAt().toEpochMilli());
			json.setItemId(itemId);
			json.setStatus(status);
			json.setProcessor("etl-ms");
			json.setMessageCount(to.getMessageCount());
			jsons.add(json);

			StatusEntity e = StatusEntity.findByItemId(itemId);
			if (e != null) {
				e.setStatus(status);
				e.persist();
			}
			// Finish work
			json.setProcessedAt(Instant.now().toEpochMilli());
		}
		try {
			httpReceiver.postStatusChange(jsons);
		} catch (Exception e) {
			Log.error(e);
		}
	}
}
