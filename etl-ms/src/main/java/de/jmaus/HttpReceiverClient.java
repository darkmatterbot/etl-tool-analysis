package de.jmaus;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import de.jmaus.json.IdentifierChangeJson;
import de.jmaus.json.LogCreationJson;
import de.jmaus.json.StatusChangeJson;
import io.quarkus.rest.client.reactive.ClientExceptionMapper;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("/rest/inbound")
@RegisterRestClient(configKey = "http-client")
public interface HttpReceiverClient {
	@POST
	@Path("/log-creation-message")
	Response postLogCreationMessage(List<LogCreationJson> logJsons);

	@POST
	@Path("/identifier-change-event")
	Response postIdentifierChange(List<IdentifierChangeJson> jsons);

	@POST
	@Path("/status-change-event")
	Response postStatusChange(List<StatusChangeJson> logJsons);

	@ClientExceptionMapper
	static RuntimeException toException(Response response) {

		if (response.getStatus() == 500) {
			return new RuntimeException("The remote service responded with HTTP 500");
		}
		if (response.getStatus() == 400) {
			return new RuntimeException("400: " + response);
		}

		if (response.getStatus() == 404) {
			return new RuntimeException("404: " + response);
		}
		return null;
	}
}