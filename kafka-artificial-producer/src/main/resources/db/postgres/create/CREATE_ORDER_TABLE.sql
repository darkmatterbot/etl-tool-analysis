CREATE TABLE IF NOT EXISTS orders
(
	id 			BIGINT PRIMARY KEY DEFAULT nextval('orders_SEQ'),
	createdat 	TIMESTAMP DEFAULT NOW() NOT NULL,
	kundenid 	BIGINT, 
	bestellid 	BIGINT
)