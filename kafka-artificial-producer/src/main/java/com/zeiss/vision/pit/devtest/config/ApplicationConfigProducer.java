package com.zeiss.vision.pit.devtest.config;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.enterprise.inject.Produces;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import io.quarkus.logging.Log;
import io.quarkus.runtime.StartupEvent;

/**
 *
 * @(#)APplicationCOnfigProducer.java
 *
 *                                    Copyright (c) 2021 - 2023 Carl Zeiss Vision GmbH Turnstrasse 27, 73430 Aalen, Germany All Rights Reserved
 *
 *                                    This software is the confidential and proprietary information of Carl Zeiss Vision GmbH ("Confidential
 *                                    Information"). You shall not disclose such Confidential Information and shall use it only in accordance with the
 *                                    terms of the license agreement you entered into with Carl Zeiss Vision GmbH
 *
 *                                    Date: 13.03.2023
 * @version: 1.0
 *
 */
@ApplicationScoped
public class ApplicationConfigProducer {

    @ConfigProperty(name = "MESSAGES_PER_SECOND", defaultValue = "1")
    String MESSAGES_PER_SECOND = "1";

    @Produces
    ApplicationConfig getConfig() {
        // convert to millis
        long millis = 1l;
        try {
            millis = (long) (1000 / Float.parseFloat(MESSAGES_PER_SECOND));
        } catch (Exception e) {
            millis = 1l;
        }
        return new ApplicationConfig(millis);
    }

    public void init(@Observes StartupEvent ev) {
        Log.info("MESSAGES_PER_SECOND: " + MESSAGES_PER_SECOND);
    }

}
