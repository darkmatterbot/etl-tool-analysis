package com.zeiss.vision.pit.devtest.persistence;

import java.time.Instant;

import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.quarkus.mongodb.panache.common.MongoEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@MongoEntity(collection = "logs")
public class LogEntity extends PanacheMongoEntity {

	public String documentId;

	public String message;

	public String value;

	public static LogEntity findByDocumentId(String id) {
		return find("documentId", id).firstResult();
	}

}
