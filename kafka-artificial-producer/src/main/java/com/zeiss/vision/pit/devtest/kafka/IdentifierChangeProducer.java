package com.zeiss.vision.pit.devtest.kafka;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Random;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

import com.zeiss.vision.pit.devtest.persistence.Auftrag;

import de.jmaus.kafka.schema.IdentifierStateTO;
import io.quarkus.logging.Log;
import jakarta.enterprise.context.RequestScoped;
import jakarta.transaction.Transactional;

@RequestScoped
public class IdentifierChangeProducer {

	private static final Random random = new Random();
//
//	@Channel("identifier-change-event")
//	private Emitter<IdentifierStateTO> emitter;
//
//	public void produceMessage(long CNT) {
//		for (int i = 0; i < CNT; i++) {
//
//			long kundenId = Integer.valueOf(random.nextInt());
//			long auftragsId = kundenId + 1;
//			insertOrdersDB(kundenId, auftragsId);
//			IdentifierStateTO to = getIdentifierRecord(kundenId, auftragsId, CNT);
//			emitter.send(to);
//		}
//	}

//	@Transactional
	public static Auftrag insertDB(long kundenId, long bestellId) {
		Auftrag e = new Auftrag();
		e.setKundenid(kundenId);
		// randomly set bestellid
		e.setBestellid(-1l);
//		if (random.nextBoolean()) {
//			e.setBestellid(-1l);
//		}
//		e.persist();
		return e;
	}

	public static IdentifierStateTO getRecord(long createdAt,long kundenId, long bestellId, long messageCNT) {
		IdentifierStateTO to = new IdentifierStateTO();
		to.setCreatedAt(Instant.ofEpochMilli(createdAt));
		to.setMessageCount(messageCNT);
		to.setKundenId(kundenId);
		to.setBestellId(bestellId);
		return to;
	}
}
