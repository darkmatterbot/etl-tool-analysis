package com.zeiss.vision.pit.devtest.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 *
 * @(#)ApplicationConfig.java
 *
 *                            Copyright (c) 2021 - 2023 Carl Zeiss Vision GmbH Turnstrasse 27, 73430 Aalen, Germany All Rights Reserved
 *
 *                            This software is the confidential and proprietary information of Carl Zeiss Vision GmbH ("Confidential Information").
 *                            You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 *                            agreement you entered into with Carl Zeiss Vision GmbH
 *
 *                            Date: 13.03.2023
 * @version: 1.0
 *
 */
@ToString
@AllArgsConstructor
public class ApplicationConfig {

    @Getter
    private final long    MESSAGES_PER_SECOND;
}
