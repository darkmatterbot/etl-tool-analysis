package com.zeiss.vision.pit.devtest;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import jakarta.inject.Singleton;

import io.quarkus.logging.Log;
import io.quarkus.runtime.annotations.RegisterForReflection;

@Singleton
@RegisterForReflection
public class ResourceLoader {

	public static String getFile(String file) {
		InputStream indexIS = null;
		try {
			indexIS = ResourceLoader.class.getClassLoader().getResourceAsStream(file);
			final InputStreamReader isr = new InputStreamReader(indexIS, StandardCharsets.UTF_8);
			final StringBuilder sb = new StringBuilder();
			int c;
			while ((c = isr.read()) != -1) {
				sb.append((char) c);
			}
			return sb.toString();
		} catch (Exception e) {
			Log.info(file + ": " + e.getLocalizedMessage());
		} finally {
			if (indexIS != null) {
				try {
					indexIS.close();
				} catch (IOException e) {
					Log.error(e);
				}
			}
		}
		return null;
	}

}
