package com.zeiss.vision.pit.devtest;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

import com.zeiss.vision.pit.devtest.kafka.IdentifierChangeProducer;
import com.zeiss.vision.pit.devtest.kafka.LogCreationProducer;
import com.zeiss.vision.pit.devtest.kafka.StatusChangeProducer;
import com.zeiss.vision.pit.devtest.persistence.Auftrag;
import com.zeiss.vision.pit.devtest.persistence.LogEntity;
import com.zeiss.vision.pit.devtest.persistence.StatusEntity;

import de.jmaus.kafka.schema.IdentifierStateTO;
import de.jmaus.kafka.schema.LogCreationMessageTO;
import de.jmaus.kafka.schema.StatusChangeTO;
import io.quarkus.logging.Log;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("/go")
public class Startup {
	private static final int CNT = 1000;

	@Channel("log-creation-message")
	private Emitter<LogCreationMessageTO> emitter;

	@Channel("identifier-change-event")
	private Emitter<IdentifierStateTO> emitter2;

	@Channel("status-change-event")
	private Emitter<StatusChangeTO> emitter3;

	static Long ID = 0l;


	@GET
	public Response x() {

		long d = LogEntity.deleteAll();
		Log.info("Deleted: " + d);
		List<LogCreationMessageTO> logTos = new ArrayList<>(CNT);
		List<IdentifierStateTO> identifierTos = new ArrayList<>(CNT);
		List<StatusChangeTO> statusTos = new ArrayList<>(CNT);

		List<LogEntity> logETos = new ArrayList<>(CNT);
		List<Auftrag> identifierETos = new ArrayList<>(CNT);
		List<StatusEntity> statusETos = new ArrayList<>(CNT);

		for (int i = 0; i < CNT; i++) {

			ID++;
			long createdAt = Instant.now().toEpochMilli();
			String documentId = String.valueOf(ID);
			String message = "Item " + i;
			logETos.add(LogCreationProducer.insertDB(documentId, message));
			logTos.add(LogCreationProducer.getRecord(createdAt, documentId, message, CNT));

			Long kundenId = ID;
			long auftragsId = kundenId + 1;
			identifierETos.add(IdentifierChangeProducer.insertDB(kundenId, auftragsId));
			identifierTos.add(IdentifierChangeProducer.getRecord(createdAt, kundenId, auftragsId, CNT));

			String itemId = "Item " + ID;
			long status = ID + 1;
			statusETos.add(StatusChangeProducer.insertDB(itemId, status));
			statusTos.add(StatusChangeProducer.getRecord(createdAt, itemId, status, CNT));
		}

		for (int i = 0; i < CNT; i += 1000) {
			int end = i + 1000;
			if (end > CNT) {
				end = CNT;
			}
//			Log.info("Insert (" + i + ", " + end + ")");
			persistPg(identifierETos.subList(i, end), statusETos.subList(i, end));
			LogEntity.mongoCollection().insertMany(logETos.subList(i, end));
		}
		for (int i = 0; i < CNT; i++) {
			emitter.send(logTos.get(i));
			emitter2.send(identifierTos.get(i));
			emitter3.send(statusTos.get(i));
		}
		Log.info("GENERATED: " + Instant.now().toEpochMilli());
		return Response.status(200).build();
	}

	@Transactional
	void persistPg(List<Auftrag> identifierETos, List<StatusEntity> statusETos) {
		Auftrag.persist(identifierETos);
		StatusEntity.persist(statusETos);
	}
}
