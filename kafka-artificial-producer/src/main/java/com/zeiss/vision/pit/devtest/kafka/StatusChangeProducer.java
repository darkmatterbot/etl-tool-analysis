package com.zeiss.vision.pit.devtest.kafka;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Random;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

import com.zeiss.vision.pit.devtest.persistence.StatusEntity;

import de.jmaus.kafka.schema.StatusChangeTO;
import jakarta.enterprise.context.RequestScoped;
import jakarta.transaction.Transactional;

@RequestScoped
public class StatusChangeProducer {

	private static final Random random = new Random();

//	@Channel("status-change-event")
//	private Emitter<StatusChangeTO> emitter;
//
//	public void produceMessage(long CNT) {
//		for (int i = 0; i < CNT; i++) {
//			int id = Integer.valueOf(random.nextInt());
//			String itemId = "Item " + id;
//			long status = id + 1;
//			insertDB(itemId, status);
//			StatusChangeTO to = getRecord(itemId, status, CNT);
//			emitter.send(to);
//		}
//	}

//	@Transactional
	public static StatusEntity insertDB(String itemId, long status) {
		StatusEntity e = new StatusEntity();
		e.setItemid(itemId);
		e.setStatus(-1l);
//		e.persist();
		return e;
	}

	public static StatusChangeTO getRecord(long createdAt, String itemId, long status, long messageCNT) {
		StatusChangeTO to = new StatusChangeTO();
		to.setCreatedAt(Instant.ofEpochMilli(createdAt));
		to.setMessageCount(messageCNT);
		to.setItemId(itemId);
		to.setStatus(status);
		return to;
	}
}
