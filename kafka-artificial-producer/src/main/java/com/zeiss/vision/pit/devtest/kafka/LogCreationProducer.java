package com.zeiss.vision.pit.devtest.kafka;

import java.time.Instant;

import com.zeiss.vision.pit.devtest.persistence.LogEntity;
import de.jmaus.kafka.schema.LogCreationMessageTO;
import jakarta.enterprise.context.RequestScoped;

@RequestScoped
public class LogCreationProducer {

	private static final String VALUE = "From MongoDB: ";

//	@Channel("log-creation-message")
//	private Emitter<LogCreationMessageTO> emitter;
//
//	public void produceLogCreationMessage(long CNT) {
//		for (int i = 0; i < CNT; i++) {
//			String documentId = Integer.valueOf(random.nextInt()).toString();
//			Instant createdAt = Instant.now();
//			String message = MESSAGE + i;
//			insertDB(documentId, createdAt, message);
//			LogCreationMessageTO to = getRecord(documentId, createdAt, message, CNT);
//			emitter.send(to);
//		}
//	}

	public static LogEntity insertDB(String documentId, String message) {
		LogEntity e = new LogEntity();

		e.setDocumentId(documentId);
		e.setMessage(message);
		e.setValue(VALUE + documentId);

//		e.persist();
		return e;
	}

	public static LogCreationMessageTO getRecord(long createdAt, String documentId, String message, long messageCNT) {
		LogCreationMessageTO to = new LogCreationMessageTO();
		to.setCreatedAt(Instant.ofEpochMilli(createdAt));
		to.setDocumentId(String.valueOf(documentId));
		to.setMessage(message);
		to.setMessageCount(messageCNT);
		return to;
	}
}
