package com.zeiss.vision.pit.devtest.analysis;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
public class Analysis {
	String event;
	List<ChartData> data;
	double mean;
	double stdev;

	public Analysis(String event) {
		this.event = event;
	}
}
