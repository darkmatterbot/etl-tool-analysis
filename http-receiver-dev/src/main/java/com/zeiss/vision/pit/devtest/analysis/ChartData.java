package com.zeiss.vision.pit.devtest.analysis;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
public class ChartData {
	double x;
	long y;
}
