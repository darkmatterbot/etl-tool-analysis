package com.zeiss.vision.pit.devtest;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zeiss.vision.pit.devtest.analysis.Analysis;
import com.zeiss.vision.pit.devtest.analysis.ChartData;

import io.quarkus.logging.Log;
import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.event.Observes;

public class JsonWriter {
	static final List<String> FILE_PATHS = Arrays.asList("1000_etl_ms_identifier-change-event_2023_05_20T12_23_49.json",
			"1000_nifi_identifier-change-event_2023_05_20T12_31_28.json",
			"1000_streamsets_dc_identifier-change-event_2023_05_20T13_54_47.json");

	static final String ONE_K_PATH = "C:\\Users\\joni\\Desktop\\thesis\\performance\\logs\\1000\\";
	static final String PATH = "C:\\Users\\joni\\Desktop\\thesis\\performance\\logs\\";
	public static final String MS = "etl-ms";
	public static final String NIFI = "nifi";
	public static final String STREAMSET_DC = "streamsets-dc";
	static final ObjectMapper objectMapper = new ObjectMapper();

	public void y(@Observes StartupEvent ev) {

//		for (String file : FILE_PATHS) {
//			try {
//				String filepath = ONE_K_PATH + file;
//				List<ChartData> cd = objectMapper.readValue(new File(filepath), new TypeReference<List<ChartData>>() {
//				});
//
//				List<ChartData> newChartData = new ArrayList<>(cd.size());
//				for (ChartData d : cd) {
//					newChartData.add(new ChartData(d.getX() / 1000.0, d.getY()));
//				}
//				objectMapper.writeValue(new File(filepath + "_SECONDS"), newChartData);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
	}

	public static void write(String processor, Analysis analysis) {
		int n = analysis.getData().size();
		String event = analysis.getEvent();

		// MEAN
		List<Long> yValues = analysis.getData().stream().map(ChartData::getY).toList();
		Long sum = yValues.stream().reduce(0l, Long::sum);
		Long mean = sum / n;
		analysis.setMean(mean);

		// STDEV
		double stdev = 0.0;
		for (Long x : yValues) {
			stdev += Math.pow(x - mean, 2);
		}
		stdev = Math.sqrt(stdev / n);
		analysis.setStdev(stdev);

		Ansi color = getColor(processor);
		Log.info(color + processor + " [" + event + "  " + n + " entries] Mean latency: "
				+ TimeUnit.MILLISECONDS.toSeconds(mean) + "s / " + mean + " ms " + " STDEV: " + stdev
				+ Ansi.ansi().reset());

		writeToFile(processor, analysis, n);
	}

	static void writeToFile(String processor, Analysis analysis, long n) {
		try {
			String filename = getFileName(processor, analysis.getEvent(), n);
//			objectMapper.writerWithDefaultPrettyPrinter().writeValue(new File(filename), analysis);
			List<Long> vals = analysis.getData().stream().map(e->e.getY()).toList();
			
			String boxplot = filename+"_BOXPLOT.json";
			objectMapper.writeValue(new File(boxplot), vals);
			objectMapper.writeValue(new File(filename), analysis.getData());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static String getFileName(String processor, String event, long n) {
		return PATH + n + "_" + processor.replace("-", "_") + "_" + event + "_" + LocalDateTime.now()
				.truncatedTo(ChronoUnit.SECONDS).toString().replace(" ", "_").replace("-", "_").replace(":", "_")
				+ ".json";
	}

	static Ansi getColor(String processor) {
		Ansi color = Ansi.ansi();
		AnsiConsole.systemInstall();
		if (processor.equals(MS))
			color = color.fgBrightBlue();
		else if (processor.equals(NIFI))
			color = color.fgYellow();
		else if (processor.equals(STREAMSET_DC))
			color = color.fgBrightCyan();
		return color;
	}
}
