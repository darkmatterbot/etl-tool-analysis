package com.zeiss.vision.pit.devtest.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseJson {
	@JsonProperty("processor")
	private String processor;

	@JsonProperty("messageCount")
	private long messageCount;
	
	@JsonProperty("createdAt")
	private long createdAt;
	
	@JsonProperty("processStart")
	private long processStart;

	@JsonProperty("processedAt")
	private long processedAt;
}
