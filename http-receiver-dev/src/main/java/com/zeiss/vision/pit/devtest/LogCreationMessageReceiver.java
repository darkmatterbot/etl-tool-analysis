package com.zeiss.vision.pit.devtest;

import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.zeiss.vision.pit.devtest.analysis.Analysis;
import com.zeiss.vision.pit.devtest.analysis.ChartData;
import com.zeiss.vision.pit.devtest.json.LogCreationJson;

import io.quarkus.logging.Log;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("/rest/inbound")
public class LogCreationMessageReceiver {
	static final String EVENT = "log-creation-message";

	static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSZ");

	static Map<String, Analysis> MAP = Map.of(JsonWriter.MS, new Analysis(EVENT), JsonWriter.NIFI, new Analysis(EVENT),
			JsonWriter.STREAMSET_DC, new Analysis(EVENT));

	@POST
	@Path("/log-creation-message")
	public Response post(List<LogCreationJson> jsons) {
		int code = 403;
		long messageCount = jsons.get(0).getMessageCount();
		String processor = jsons.get(0).getProcessor();
		if (jsons != null) {
			for (LogCreationJson json : jsons) {
				if (json == null) {
					code = 422;
					Log.error("Missing values: " + json);
					continue;
				} else {
					code = 201;
					ChartData d = new ChartData();
					Instant startProcess = Instant.ofEpochMilli(json.getProcessStart());
					Instant endProcess = Instant.ofEpochMilli(json.getProcessedAt());
					long duration = Duration.between(startProcess, endProcess).toMillis();
					d.setX(json.getProcessedAt() - json.getCreatedAt());
//					d.setX(String.valueOf(json.getProcessedAt() - json.getCreatedAt()));
					d.setY(duration);

					try {
						if (MAP.get(processor).getData() == null || MAP.get(processor).getData().isEmpty()) {
							MAP.get(processor).setData(new ArrayList<>());
						}

						MAP.get(processor).getData().add(d);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			Analysis data = MAP.get(processor);
			if (MAP.get(processor).getData() != null && !MAP.get(processor).getData().isEmpty()) {
				analyse(processor, data, messageCount);
			}

		}
		return Response.status(code).build();
	}

	void analyse(String processor, Analysis analysis, long messageCnt) {
		int currentSize = analysis.getData().size();
		if (currentSize > 0 && currentSize % messageCnt == 0) {
			JsonWriter.write(processor, analysis);
			analysis.getData().clear();
		}
	}
}