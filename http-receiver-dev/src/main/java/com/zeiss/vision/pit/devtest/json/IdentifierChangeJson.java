package com.zeiss.vision.pit.devtest.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
public class IdentifierChangeJson extends BaseJson {
	@JsonProperty("kundenId")
	private long kundenId;

	@JsonProperty("bestellId")
	private long bestellId;

	@JsonProperty("startDate")
	private String startDate;
}
