package com.zeiss.vision.pit.devtest.persistence;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.runtime.annotations.RegisterForReflection;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "orders")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@RegisterForReflection
public class Auftrag extends PanacheEntityBase {
	@Id
	private Long kundenid;
	private Long bestellid;
	private long processStart;
	private long updated_at;
	
	public static Auftrag findByKundenId(Long kundenid) {
		return find("kundenid", kundenid).firstResult();
	}

	public static Auftrag findByBestellId(Long bestellId) {
		return find("bestellid", bestellId).firstResult();
	}
}
