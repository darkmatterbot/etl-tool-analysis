package com.zeiss.vision.pit.devtest.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
public class StatusChangeJson extends BaseJson {

	@JsonProperty("itemId")
	private String itemId;

	@JsonProperty("status")
	private Long status;

}
