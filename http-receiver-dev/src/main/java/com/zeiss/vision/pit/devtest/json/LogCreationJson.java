package com.zeiss.vision.pit.devtest.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
public class LogCreationJson extends BaseJson {

	@JsonProperty("documentId")
	private String documentId;

	@JsonProperty("message")
	private String message;

	@JsonProperty("value")
	private String value;
}
