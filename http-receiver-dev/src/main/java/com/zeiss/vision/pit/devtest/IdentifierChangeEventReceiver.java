package com.zeiss.vision.pit.devtest;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.zeiss.vision.pit.devtest.analysis.Analysis;
import com.zeiss.vision.pit.devtest.analysis.ChartData;
import com.zeiss.vision.pit.devtest.json.IdentifierChangeJson;
import com.zeiss.vision.pit.devtest.persistence.Auftrag;

import io.quarkus.logging.Log;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("/rest/inbound")
public class IdentifierChangeEventReceiver {
	static final String EVENT = "identifier-change-event";

	static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSZ");

	static Map<String, Analysis> MAP = Map.of(JsonWriter.MS, new Analysis(EVENT), JsonWriter.NIFI, new Analysis(EVENT),
			JsonWriter.STREAMSET_DC, new Analysis(EVENT));

	@POST
	@Path("/identifier-change-event")
	public Response post(List<IdentifierChangeJson> jsons) {
		int code = 403;
		long messageCount = jsons.get(0).getMessageCount();
		String processor = jsons.get(0).getProcessor();
		if (jsons != null) {
			for (IdentifierChangeJson json : jsons) {
				if (json == null || json.getCreatedAt() == 0 || json.getMessageCount() <= 0) {
					code = 422;
					Log.error("Missing values: " + json);
					continue;
				} else {
					code = 201;
					ChartData d = new ChartData();

					long y = json.getProcessedAt() - json.getProcessStart();
					long x = json.getProcessedAt() - json.getCreatedAt();

					if (processor.equals(JsonWriter.NIFI)) {
//						Auftrag e = Auftrag.findByKundenId(json.getKundenId());
//						y = e.getUpdated_at() - e.getProcessStart();
						y = json.getProcessedAt() - json.getProcessStart();
					}
					d.setX(x / 1000.0);
					d.setY(y);
//					Log.info("(x: " + x + ", y: " + y + ")");
//					Log.info(json.getProcessStart() + " -- " + json.getProcessedAt() + " == " + y);
					try {
						if (MAP.get(processor).getData() == null || MAP.get(processor).getData().isEmpty()) {
							MAP.get(processor).setData(new ArrayList<>());
						}
						MAP.get(processor).getData().add(d);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			Analysis data = MAP.get(processor);
			if (MAP.get(processor).getData() != null && !MAP.get(processor).getData().isEmpty()) {
				analyse(processor, data, messageCount);
			}
		}
		return Response.status(code).build();
	}

	void analyse(String processor, Analysis analysis, long messageCnt) {
		int currentSize = analysis.getData().size();
		if (currentSize > 0 && currentSize % messageCnt == 0) {
			Collections.sort(analysis.getData(), Comparator.comparing(ChartData::getX));
			JsonWriter.write(processor, analysis);
			analysis.getData().clear();
		}
	}
}